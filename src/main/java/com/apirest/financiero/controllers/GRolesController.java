package com.apirest.financiero.controllers;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apirest.financiero.models.entity.Roles;
import com.apirest.financiero.models.services.GRolesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin({"*"})
@RequestMapping("/api/v1")
public  class GRolesController {

    @Autowired
    private GRolesService gRolesService;


    @GetMapping("/roles/list")
    public List<Roles> findAll(){
        return gRolesService.findAll();
    }
    
    @GetMapping("/roles/{id}")
    public Roles findById(@PathVariable("id") Integer id){
        return gRolesService.findById(id);
    }

    @PostMapping("/roles/new")
    public ResponseEntity<?> create(@RequestBody Roles roles) {
        Map<String, Object> response = new HashMap<>();
        Roles rolesNew = null;

        try {
            rolesNew = gRolesService.create(roles);

        } catch (DataAccessException e) {

            response.put("mensaje", "Error al Crear el usuario " + roles.getNombre());
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "El usuario: " + rolesNew.getId() + " con el Nombre " + rolesNew.getNombre() + " ha sido creado con éxito!");
        response.put("Entidad", rolesNew);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}