package com.apirest.financiero.controllers;

import com.apirest.financiero.models.entity.PSolicitudCdp;
import com.apirest.financiero.models.entity.PSolicitudCdpPK;
import com.apirest.financiero.models.services.PSolicitudCdpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin({"*"})
@RequestMapping("/api/v1")
public class PSolicitudCdpController {
    @Autowired
    private PSolicitudCdpService pSolicitudCdpService;

    @GetMapping("/solicitudcdp/list")
    public List<PSolicitudCdp> findAll() {
        return pSolicitudCdpService.findAll();
    }


    @PostMapping("/solicitudcdp/pk")
    public ResponseEntity<?> show(@RequestBody PSolicitudCdpPK pSolicitudCdpPK) {
        PSolicitudCdp pSolicitudCdp;
        Map<String, Object> response = new HashMap<>();

        try {
            pSolicitudCdp = pSolicitudCdpService.findById(pSolicitudCdpPK);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la consulta");
            response.put("error", e.getMessage() + ": " + (e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (pSolicitudCdp == null) {
            response.put("mensaje", "El comprobante contable detalle  no existe");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<PSolicitudCdp>(pSolicitudCdp, HttpStatus.OK);
    }


    @PostMapping("/solicitudcdp/new")
    public ResponseEntity<?> create(@RequestBody PSolicitudCdp pSolicitudCdp) {

        PSolicitudCdp pSolicitudCdpNew;
        Map<String, Object> response = new HashMap<>();

        try {
            pSolicitudCdpNew = pSolicitudCdpService.save(pSolicitudCdp);
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al realizar la inserción");
            response.put("error", e.getMessage() + ": " + (e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        response.put("mensaje", "El comprobante contable detalle ha sido creado con éxito!");
        response.put("comprobanteContable", pSolicitudCdpNew);
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }


    @PutMapping("/solicitudcdp/updated")
    public ResponseEntity<?> update(@RequestBody PSolicitudCdp pSolicitudCdp) {
        PSolicitudCdp pSolicitudCdpUpdated;

        Map<String, Object> response = new HashMap<>();

        if (pSolicitudCdp == null) {
            response.put("mensaje", "Error: no se puede editar, el comprobanteContDetActual No existe");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        try {
            if(pSolicitudCdp != null) {
                response.put("ordendepago", pSolicitudCdpService.save(pSolicitudCdp));
            }

        } catch (DataAccessException e) {
            response.put("mensaje", "Error al actualizar el comprobante contable detalle");
            response.put("error", e.getMessage()+": "+(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("mensaje", "El comprobante contable detalle ha sido actualizado con éxito!");

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

}
