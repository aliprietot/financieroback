package com.apirest.financiero.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apirest.financiero.models.entity.GTerceroPtal;
import com.apirest.financiero.models.services.GTerceroPtalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin({"*"})
@RequestMapping("/api/v1")

public class GTerceroPtalController {

    @Autowired
    private GTerceroPtalService gTerceroPtalService;

    @GetMapping("/gterceroptal")
    public List<GTerceroPtal> findAll(){
        return gTerceroPtalService.findAll();
    }

    @GetMapping("/gterceroptal/{id}")
    public GTerceroPtal findById(@PathVariable("id") Long id){
        return gTerceroPtalService.findById(id);
    }

    @GetMapping("/gterceroptal/codigo/{terCod}")
    public GTerceroPtal findByTerCod(@PathVariable("terCod") Long terCod){
        return gTerceroPtalService.findByTerCod(terCod);
    }

   @PostMapping("/gterceroptal/new")
   public ResponseEntity<?> create(@RequestBody GTerceroPtal gTerceroPtal){
    GTerceroPtal gTerceroPtalNew;
    Map<String, Object> response = new HashMap<>();
    try {
        gTerceroPtalNew = gTerceroPtalService.save(gTerceroPtal);
    } catch (DataAccessException e) {
        response.put("mensaje", "Error al realizar la inserción");
        response.put("error", e.getMessage() + ": " + (e.getMostSpecificCause().getMessage()));
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    response.put("mensaje", "El comprobante contable detalle ha sido creado con éxito!");
    response.put("comprobanteContable", gTerceroPtalNew);
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
   } 

   @PutMapping("/gterceroptal/{id}")
   public ResponseEntity<?> update(@RequestBody GTerceroPtal gTerceroPtal, @PathVariable("id") Long id ){
    GTerceroPtal gTerceroPtalActual = gTerceroPtalService.findById(id);
    GTerceroPtal gTerceroPtalUpdated;

    Map<String, Object> response = new HashMap<>();

    if (gTerceroPtalActual == null) {
        response.put("mensaje", "Error: no se puede editar, el comprobanteContDetActual No existe");
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
    }
    try {
        gTerceroPtalUpdated  = gTerceroPtalService.save(gTerceroPtal);
    } catch (DataAccessException e) {
        response.put("mensaje", "Error al actualizar el comprobante contable detalle");
        response.put("error", e.getMessage()+": "+(e.getMostSpecificCause().getMessage()));
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    response.put("mensaje", "El comprobante contable detalle ha sido actualizado con éxito!");
    response.put("comprobcontdet", gTerceroPtalUpdated);
    return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
   }
}