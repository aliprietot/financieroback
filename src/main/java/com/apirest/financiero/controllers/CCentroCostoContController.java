package com.apirest.financiero.controllers;

import com.apirest.financiero.models.entity.CCentroCostoCont;
import com.apirest.financiero.models.entity.CCentroCostoContPK;
import com.apirest.financiero.models.services.CCentroCostContService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = {"*"})
@RequestMapping("/api/v1")
public class CCentroCostoContController {

	@Autowired
	private CCentroCostContService centroCostoServ;

	@GetMapping("/centrocosto/list")
	public List<CCentroCostoCont> findAll() {
		return centroCostoServ.findAll();
	}

	@GetMapping("/centrocosto/{empCod}/{cenCod}")
	public ResponseEntity<?> show(@PathVariable("empCod") String empCod,
																@PathVariable("cenCod") String cenCod, CCentroCostoContPK centroCostoPk) {
		centroCostoPk.setEmpCod(empCod);
		centroCostoPk.setCenCod(cenCod);

		CCentroCostoCont centroCosto;
		Map<String, Object> response = new HashMap<>();

		try {
			centroCosto = centroCostoServ.findById(centroCostoPk);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta");
			response.put("error", e.getMessage() + ": " + (e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (centroCosto == null) {
			response.put("mensaje", "El centro de costo con el ID:".concat(centroCostoPk.toString().concat(" no existe")));
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(centroCosto, HttpStatus.OK);
	}

	@PutMapping("/centrocosto/update")
	public ResponseEntity<?> update(@RequestBody CCentroCostoCont centroCosto) {
		CCentroCostoCont centroCostoAct = centroCostoServ.findById(centroCosto.getCCentroCostoContPK());
		Map<String, Object> response = new HashMap<>();

		if (centroCostoAct == null) {
			response.put("mensaje", "Error: no se puede editar, el centro de costo No existe");
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		try {
			response.put("centrocosto", centroCostoServ.save(centroCosto));
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el centro de costo contable");
			response.put("error", e.getMessage() + ": " + (e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El centro de costo ha sido actualizado con éxito!");
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@PostMapping("/centrocosto/new")
	public ResponseEntity<?> create(@RequestBody CCentroCostoCont centroCosto) {
		Map<String, Object> response = new HashMap<>();

		try {
			response.put("centrocosto", centroCostoServ.save(centroCosto));
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta");
			response.put("error", e.getMessage() + ": " + (e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El centro de costo ha sido creado con éxito!");
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

}
