package com.apirest.financiero.models.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Embeddable
public class PSolicitudCdpPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "AnoCod")
    private short anoCod;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ScdpCod")
    private long scdpCod;
}
