/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apirest.financiero.models.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 *
 * @author jonathan mendoza
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@Entity
@Table(name = "C_CentroCostoCont")

public class CCentroCostoCont implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CCentroCostoContPK cCentroCostoContPK;
    @Basic(optional = false)
    @Column(name = "CenDes")
    private String cenDes;
    @Basic(optional = false)
    @Column(name = "estado")
    private boolean estado;


}
