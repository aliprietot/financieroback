package com.apirest.financiero.models.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "PG_SolicitudCDP")
public class PSolicitudCdp implements Serializable {

    @EmbeddedId
    protected PSolicitudCdpPK pSolicitudCdpPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ScdpFec")
    @Temporal(TemporalType.DATE)
    private Date scdpFec;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "ScdpHor")
    private String scdpHor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "FuncCodS")
    private String funcCodS;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "SoliCod")
    private String soliCod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "ScdpSust")
    private String scdpSust;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "ScdpCpt")
    private String scdpCpt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ScdpVlr")
    private BigDecimal scdpVlr;
    @Basic(optional = false)
    @Column(name = "ScdpVlrQueda")
    private BigDecimal scdpVlrQueda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "ScdpUsuElab")
    private String scdpUsuElab;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "ScdpUsuApr")
    private String scdpUsuApr;
    @Basic(optional = false)
    @Column(name = "ScdpFecApr")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scdpFecApr;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "ScdpUsuAnu")
    private String scdpUsuAnu;
    @Basic(optional = false)
    @Column(name = "ScdpFecAnu")
    @Temporal(TemporalType.TIMESTAMP)
    private Date scdpFecAnu;
    @Basic(optional = false)
    @Size(min = 1, max = 8)
    @Column(name = "ScdpHorAnu")
    private String scdpHorAnu;
    @Basic(optional = false)
    @Size(min = 1, max = 1000)
    @Column(name = "ScdpCsaAnu")
    private String scdpCsaAnu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "OrdCodS")
    private String ordCodS;
    @Basic(optional = false)
    @Size(min = 1, max = 3)
    @Column(name = "SolCenCod")
    private String solCenCod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "EntSolicitud")
    private String entSolicitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ScdpEst")
    private String scdpEst;
    @Size(max = 30)
    @Column(name = "ScdpAltCod")
    private String scdpAltCod;
    @Column(name = "ScdpAutEst")
    private Boolean scdpAutEst;
    
    private static final long serialVersionUID = 1L;
}
