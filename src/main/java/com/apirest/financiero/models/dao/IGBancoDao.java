package com.apirest.financiero.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.apirest.financiero.models.entity.GBanco;
import com.apirest.financiero.models.entity.GBancoPK;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IGBancoDao extends JpaRepository<GBanco, GBancoPK> {

    /* @Query(value="SELECT * FROM C_Egreso where EgrCod = :egrCod", nativeQuery = true)
    public GBanco findByEgrCod(@Param("egrCod") Integer egrCod); */

}