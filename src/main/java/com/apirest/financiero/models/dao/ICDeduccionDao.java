package com.apirest.financiero.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.apirest.financiero.models.entity.CDeduccion;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ICDeduccionDao extends JpaRepository<CDeduccion, Short> {

    @Query("SELECT d FROM CDeduccion d WHERE d.dedCod = :dedCod")
    public CDeduccion findByDedCod(@Param("dedCod") Short dedCod);
    
}