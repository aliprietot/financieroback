package com.apirest.financiero.models.dao;

import com.apirest.financiero.models.entity.GProyecto;
import com.apirest.financiero.models.entity.GProyectoPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IGProyectoDao extends JpaRepository<GProyecto, GProyectoPK>{
    
}