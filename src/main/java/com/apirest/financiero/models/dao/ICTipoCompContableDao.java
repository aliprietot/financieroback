package com.apirest.financiero.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.apirest.financiero.models.entity.CTipoCompContable;
import com.apirest.financiero.models.entity.CTipoCompContablePK;

public interface ICTipoCompContableDao extends JpaRepository<CTipoCompContable, CTipoCompContablePK> {
    
}