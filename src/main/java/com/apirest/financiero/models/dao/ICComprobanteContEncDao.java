package com.apirest.financiero.models.dao;

import com.apirest.financiero.models.entity.CComprobanteContEnc;
import com.apirest.financiero.models.entity.CComprobanteContEncPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICComprobanteContEncDao extends JpaRepository<CComprobanteContEnc, CComprobanteContEncPK>{
    
}