package com.apirest.financiero.models.dao;

import com.apirest.financiero.models.entity.GDependencia;
import com.apirest.financiero.models.entity.GDependenciaPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IGDependenciaDao extends JpaRepository<GDependencia, GDependenciaPK>{
    
}