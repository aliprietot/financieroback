package com.apirest.financiero.models.dao;

import com.apirest.financiero.models.entity.PSolicitudCdp;
import com.apirest.financiero.models.entity.PSolicitudCdpPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPSolicitudCdpDao extends JpaRepository<PSolicitudCdp, PSolicitudCdpPK> {
}
