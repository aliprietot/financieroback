package com.apirest.financiero.models.services;

import com.apirest.financiero.models.entity.PSolicitudCdp;
import com.apirest.financiero.models.entity.PSolicitudCdpPK;

import java.util.List;

public interface IPSolicitudCdpService {
    List<PSolicitudCdp> findAll();

    PSolicitudCdp findById(PSolicitudCdpPK pSolicitudCdpPK);

    PSolicitudCdp save(PSolicitudCdp pSolicitudCdp);

    void deleteById(PSolicitudCdpPK pSolicitudCdpPK);
}
