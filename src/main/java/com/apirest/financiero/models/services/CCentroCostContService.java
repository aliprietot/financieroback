package com.apirest.financiero.models.services;

import com.apirest.financiero.models.dao.ICCentroCostContDao;
import com.apirest.financiero.models.entity.CCentroCostoCont;
import com.apirest.financiero.models.entity.CCentroCostoContPK;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CCentroCostContService implements ICCentroCostContService{

    @Autowired
    private ICCentroCostContDao centroCostoDao;
    
    @Override
    public List<CCentroCostoCont> findAll() {
        return centroCostoDao.findAll();
    }

    @Override
    public CCentroCostoCont findById(CCentroCostoContPK centroCostoPk) {
        return centroCostoDao.findById(centroCostoPk).orElse(null);
    }

    @Override
    public CCentroCostoCont save(CCentroCostoCont centroCosto) {
        return centroCostoDao.save(centroCosto);
    }

    @Override
    public void deleteById(CCentroCostoContPK centroCostoPk) {
        centroCostoDao.deleteById(centroCostoPk);
    }
    
}
