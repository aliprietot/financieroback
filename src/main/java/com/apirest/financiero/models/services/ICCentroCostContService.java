package com.apirest.financiero.models.services;

import com.apirest.financiero.models.entity.CCentroCostoCont;
import com.apirest.financiero.models.entity.CCentroCostoContPK;
import java.util.List;

public interface ICCentroCostContService {
    public List<CCentroCostoCont> findAll();

    public CCentroCostoCont findById(CCentroCostoContPK centroCostoPk);

    public CCentroCostoCont save(CCentroCostoCont centroCosto);

    public void deleteById(CCentroCostoContPK centroCostoPk);
}
