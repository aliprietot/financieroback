package com.apirest.financiero.models.services;

import com.apirest.financiero.models.dao.IPSolicitudCdpDao;
import com.apirest.financiero.models.entity.PSolicitudCdp;
import com.apirest.financiero.models.entity.PSolicitudCdpPK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PSolicitudCdpService implements IPSolicitudCdpService {
    @Autowired
    private IPSolicitudCdpDao iPSolicitudCdpDao;

    @Override
    public List<PSolicitudCdp> findAll() {
        return iPSolicitudCdpDao.findAll();
    }

    @Override
    public PSolicitudCdp findById(PSolicitudCdpPK pSolicitudCdpPK) {
        return iPSolicitudCdpDao.findById(pSolicitudCdpPK).orElse(null);
    }

    @Override
    public PSolicitudCdp save(PSolicitudCdp pSolicitudCdp) {
        return iPSolicitudCdpDao.save(pSolicitudCdp);
    }

    @Override
    public void deleteById(PSolicitudCdpPK pSolicitudCdpPK) {
        iPSolicitudCdpDao.deleteById(pSolicitudCdpPK);
    }
}
